# Travel Buddy (Front-end)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.9.

Back-end: https://gitlab.com/GSaunier/nwt-back.git

## Prérequis
```bash
Installer [NodeJs](https://nodejs.org/en/)
```
## Mise en place

```bash
- `git clone https://gitlab.com/GSaunier/nwt-front.git`

- `npm install -g npm@latest`

- `npm install -g yarn`

- `yarn global add @angular/cli`

- Se rendre dans le dossier racine du projet front-end

- `ng config –global cli.packageManager yarn`

- `yarn install`

```

## Exécution

```bash
- `npm run start`
```

## Connexion Admin sur le site

```bash
    Aprés lancement du back et de la base de données
    id: admin
    mdp: pass
```

## Présentation

  .   https://docs.google.com/presentation/d/1Kw_iqH-Net3pVFUHyl9SCadW-KDBf3-92BsMGRrT848/edit?usp=sharing
    
   Ou pdf de la présentation disponible dans le dossier "src/assets/Travel Buddy.pdf"
