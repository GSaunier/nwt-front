// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  backend: {
    protocol: 'http',
    host: '127.0.0.1',
    port: '3000',
    endpoints: {
      // auth
      singIn: '/auth/login',
      singUp: '/auth/register',

      // Destination
      destinations: '/destinations',
      destination: '/destinations/:id',
      destinationImage: '/destinations/:id/image',

      travelers: '/destinations/:id/travelers',
      updateTraveler: '/destinations/:id/travelers/:idProperty',

      updateOpinions: '/destinations/:id/opinions',

      profile: '/auth/profile',

      //user
      users: '/users',
      user: '/users/:id'

    }
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
