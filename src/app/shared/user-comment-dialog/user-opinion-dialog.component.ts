import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DestinationOpinion} from '../../models/destinationopinion';

@Component({
  selector: 'app-user-comment-dialog',
  templateUrl: './user-opinion-dialog.component.html',
  styleUrls: ['./user-opinion-dialog.component.scss']
})
export class UserOpinionDialogComponent implements OnInit {


  /**
   * Component constructor
   */
  constructor(private _dialogRef: MatDialogRef<UserOpinionDialogComponent>, @Inject(MAT_DIALOG_DATA) private _opinion: DestinationOpinion) {
  }

  /**
   * Returns person passed in user-dialog open
   */
  get opinion(): DestinationOpinion {
    return this._opinion;
  }

  /**
   * OnInit implementation
   */
  ngOnInit() {
  }

  /**
   * Function to cancel the process and close the modal
   */
  onCancel() {
    this._dialogRef.close();
  }

  /**
   * Function to close the modal and send person to parent
   */
  onSave(opinion: DestinationOpinion) {
    this._dialogRef.close(opinion);
  }

}
