import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {User} from '../../models/user';


@Component({
  selector: 'app-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.scss']
})
export class UserDialogComponent implements OnInit {


  /**
   * Component constructor
   */
  constructor(private _dialogRef: MatDialogRef<UserDialogComponent>, @Inject(MAT_DIALOG_DATA) private _user: User) {
  }

  /**
   * Returns person passed in user-dialog open
   */
  get user(): User {
    return this._user;
  }

  /**
   * OnInit implementation
   */
  ngOnInit() {
  }

  /**
   * Function to cancel the process and close the modal
   */
  onCancel() {
    this._dialogRef.close();
  }

  /**
   * Function to close the modal and send person to parent
   */
  onSave(user: User) {
    this._dialogRef.close(user);
  }

}
