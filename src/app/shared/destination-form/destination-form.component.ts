import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Destination} from '../../models/destination';
import {Role} from '../../models/role';
import {DestinationService} from '../services/destination.service';
import {User} from '../../models/user';
import {AuthService} from '../services/auth.service';


@Component({
  selector: 'app-destination-form',
  templateUrl: './destination-form.component.html',
  styleUrls: ['./destination-form.component.scss']
})
export class DestinationFormComponent implements OnInit, OnChanges {

  // private property to store update mode flag
  private _isUpdateMode: boolean;
  // private property to store model value
  private _model: Destination;
  // private property to store cancel$ value
  private readonly _cancel$: EventEmitter<void>;
  // private property to store submit$ value
  private readonly _submit$: EventEmitter<Destination>;
  // private property to store destination-form value
  private readonly _form: FormGroup;
  // private property to store Selected value
  private _selected: any;

  private _profile: User;

  private _fileToUpload;

  /**
   * Component constructor
   */
  constructor(private _DService: DestinationService, private _authService: AuthService) {
    this._submit$ = new EventEmitter<Destination>();
    this._cancel$ = new EventEmitter<void>();
    this._form = this._buildForm();
    this._profile = {} as User;
  }

  /**
   * Sets private property _model
   */
  @Input()
  set model(model: Destination) {
    this._model = model;
  }

  /**
   * Returns private property _model
   */
  get model(): Destination {
    return this._model;
  }

  /**
   * Returns private property _form
   */
  get form(): FormGroup {
    return this._form;
  }

  /**
   * Returns private property _isUpdateMode
   */
  isUpdateMode(): boolean {
    return this._isUpdateMode;
  }

  /**
   * Returns private property _cancel$
   */
  @Output('cancel')
  get cancel$(): EventEmitter<void> {
    return this._cancel$;
  }

  /**
   * Returns private property _submit$
   */
  @Output('submit')
  get submit$(): EventEmitter<Destination> {
    return this._submit$;
  }



  /**
   * Sets private property _selected
   */
  set selected(role: Role) {
    this._selected = role;
  }

  /**
   * OnInit implementation
   */
  ngOnInit() {
    this._authService.profile().subscribe( (user: User) => this._profile, error => console.log(error))
  }

  /**
   * Function to handle component update
   */
  ngOnChanges(record) {

    if (record.model && record.model.currentValue) {
      this._model = record.model.currentValue;
      this._isUpdateMode = true;
      this._form.patchValue(this._model);
    } else {
      this._model = {
        id: '',
        country: '',
        region: '',
        city: '',
        description: '',
        image: null
      };
      this._isUpdateMode = false;
    }
  }

  /**
   * Function to emit event to cancel process
   */
  cancel() {
    this._cancel$.emit();
  }

  /**
   * Function to emit event to submit user-form and person
   */
  submit(destination: Destination) {
    this._submit$.emit(destination);
  }

  handleFileInput(files: FileList) {
    this._fileToUpload = files.item(0);
    this.uploadFileToActivity()
  }

  uploadFileToActivity() {
    this._DService.postFile(this._fileToUpload, this._model.id).subscribe(data => {
      // do something, if upload success
    }, error => {
      console.log(error);
    });
  }


  /**
   * Function to build our user-form
   */
  private _buildForm(): FormGroup {
    return new FormGroup({
      id: new FormControl('0'),
      country: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(3)
      ])),
      region: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(3)
      ])),
      city: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(3)
      ])),
      description: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(6)
      ])),
      image: new FormControl('', Validators.compose([

      ]))
    });
  }

}
