import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Destination} from '../../models/destination';

@Component({
  selector: 'app-destination-dialog',
  templateUrl: './destination-dialog.component.html',
  styleUrls: ['./destination-dialog.component.scss']
})
export class DestinationDialogComponent implements OnInit {

  /**
   * Component constructor
   */
  constructor(private _dialogRef: MatDialogRef<DestinationDialogComponent>, @Inject(MAT_DIALOG_DATA) private _destination: Destination) {
  }

  /**
   * Returns person passed in destination-dialog open
   */
  get destination(): Destination {
    return this._destination;
  }

  /**
   * OnInit implementation
   */
  ngOnInit() {
  }

  /**
   * Function to cancel the process and close the modal
   */
  onCancel() {
    this._dialogRef.close();
  }

  /**
   * Function to close the modal and send destination to parent
   */
  onSave(destination: Destination) {
    this._dialogRef.close(destination);
  }
}
