import { Injectable } from '@angular/core';
import {Destination} from '../../models/destination';
import {Observable} from 'rxjs';
import {DestinationOpinion} from '../../models/destinationopinion';
import {defaultIfEmpty, filter, map} from 'rxjs/operators';
import {User} from '../../models/user';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OpinionService {

  // private property to store all backend URLs
  private readonly _backendURL: any;


  constructor(private _http: HttpClient) {

    this._backendURL = {};

    // build backend base url
    let baseUrl = `${environment.backend.protocol}://${environment.backend.host}`;
    if (environment.backend.port) {
      baseUrl += `:${environment.backend.port}`;
    }

    // build all backend urls
    Object.keys(environment.backend.endpoints).forEach(k => this._backendURL[ k ] = `${baseUrl}${environment.backend.endpoints[ k ]}`);
  }



  /**
   * Function to return list of comments
   */
  fetchOpinions(dest: Destination): Observable<DestinationOpinion[]> {
    return this._http.get<DestinationOpinion[]>(this._backendURL.destination.replace(':id', dest.id).concat('/opinions'))
      .pipe(
        filter(_ => !!_),
        defaultIfEmpty([])
      );
  }


  /**
   * Function to create a new person
   */
  create(destID: string, opinion: DestinationOpinion): Observable<any> {
    return this._http.post<DestinationOpinion>(this._backendURL.destination.replace(':id', destID).concat('/opinions/' + destID), opinion, this._options());
  }


  /**
   * Function to delete one person for current id
   */
  delete(destID: string, id: string): Observable<string> {
    return this._http.delete(this._backendURL.destination.replace(':id', destID).concat('/opinions/' + id))
      .pipe(
        map(_ => id)
      );
  }

  /**
   * Function to return request options
   */
  private _options(headerList: object = {}): any {
    return { headers: new HttpHeaders(Object.assign({ 'Content-Type': 'application/json' }, headerList)) };
  }

}
