import { Injectable } from '@angular/core';
import {User} from '../../models/user';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {defaultIfEmpty, filter, map} from 'rxjs/operators';
import {DestinationOpinion} from '../../models/destinationopinion';
import {Destination} from '../../models/destination';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // private property to store all backend URLs
  private readonly _backendURL: any;
  // private property to store default person
  private readonly _defaultUser: User;

  constructor(private _http: HttpClient) {

    this._defaultUser = {
      pseudo: 'test',
      password: 'pass',
      firstname: 'firstname',
      lastname: 'lastname',
      email: 'email@ema.il',
      role: 'user'
    };
    this._backendURL = {};

    // build backend base url
    let baseUrl = `${environment.backend.protocol}://${environment.backend.host}`;
    if (environment.backend.port) {
      baseUrl += `:${environment.backend.port}`;
    }

    // build all backend urls
    Object.keys(environment.backend.endpoints).forEach(k => this._backendURL[ k ] = `${baseUrl}${environment.backend.endpoints[ k ]}`);
  }

  /**
   * Returns the default person value
   */
  get defaultPerson(): User {
    return this._defaultUser;
  }

  /**
   * Function to return list of person
   */
  fetch(): Observable<User[]> {
    return this._http.get<User[]>(this._backendURL.users)
      .pipe(
        filter(_ => !!_),
        defaultIfEmpty([])
      );
  }

  /**
   * Function to return one person for current id
   */
  fetchOne(id: string): Observable<User> {
    return this._http.get<User>(this._backendURL.user.replace(':id', id));
  }

  /**
   * Function to create a new person
   */
  create(user: User): Observable<any> {
    delete user["id"];
    return this._http.post<User>(this._backendURL.users, user, this._options());
  }

  /**
   * Function to update one persons
   */
  update(user: User): Observable<any> {
    let id = user.id;
    delete user["id"];
    return this._http.put<User>(
      this._backendURL.user.replace(':id', id), user, this._options());
  }

  /**
   * Function to delete one person for current id
   */
  delete(id: string): Observable<string> {
    return this._http.delete(this._backendURL.user.replace(':id', id))
      .pipe(
        map(_ => id)
      );
  }



  /**
   * Function to return request options
   */
  private _options(headerList: object = {}): any {
    return { headers: new HttpHeaders(Object.assign({ 'Content-Type': 'application/json' }, headerList)) };
  }
}
