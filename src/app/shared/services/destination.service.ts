import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Destination} from '../../models/destination';
import {Observable} from 'rxjs';
import {defaultIfEmpty, filter, map} from 'rxjs/operators';
import {DestinationUser} from '../../models/destinationUser';
import {DestinationImage} from '../../models/destinationImage';
import {DestinationOpinion} from '../../models/destinationopinion';



@Injectable({
  providedIn: 'root'
})
export class DestinationService {



  constructor(private _http: HttpClient ) {

    this._Destination = {
      id : " ",
      city: 'Amsterdam',
      region: 'Test',
      country: 'Pays-Bas',
      description: 'Awsome country',
      image: null,
      contacts: DestinationUser[''],
      travelers: DestinationUser['']
    };

    this._backendURL = {};

    // build backend base url
    let baseUrl = `${environment.backend.protocol}://${environment.backend.host}`;
    if (environment.backend.port) {
      baseUrl += `:${environment.backend.port}`;
    }

    // build all backend urls
    Object.keys(environment.backend.endpoints).forEach(k => this._backendURL[ k ] = `${baseUrl}${environment.backend.endpoints[ k ]}`);
  }


  /**
   * Returns the destination value
   */
  get Destination(): Destination {
    return this._Destination;
  }

  // private property to store all backend URLs
  private readonly _backendURL: any;
  // private property to store default person
  private readonly _Destination: Destination;

  /**
   * Function to create a new destination
   */


  /**
   * Function to return list of destinations
   */
  fetch(): Observable<Destination[]> {
    return this._http.get<Destination[]>(this._backendURL.destinations)
      .pipe(
        filter(_ => !!_),
        defaultIfEmpty([])
      );
  }

  fetchImage(id:string): Observable<Blob> {
    return  this._http.get(this._backendURL.destinationImage.replace(':id', id),{ responseType: 'blob' });
  }



  /**
   * Function to return one destination for current id
   */
  fetchOne(id: string): Observable<Destination> {
    return this._http.get<Destination>(this._backendURL.destination.replace(':id', id));
  }

  create(dest: Destination): Observable<any> {
    delete dest["id"];
    delete dest["image"];
    return this._http.post<Destination>(this._backendURL.destinations, dest, this._options());
  }

  /**
   * Function to update one destination
   */
  update(dest: Destination): Observable<any> {
    let id: string = dest.id;
    delete dest["id"];
    delete dest["image"];
    console.error(dest);
    return this._http.put<Destination>(this._backendURL.destination.replace(':id', id), dest, this._options());
  }

  updateImage(dest: Destination, image): Observable<any> {
    return this._http.post<Destination>(this._backendURL.destinationImage.replace(':id', dest.id ),image)
  }

  postFile(fileToUpload: File,id: string): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('fileKey', fileToUpload, fileToUpload.name);
    return this._http.post(this._backendURL.destinationImage.replace(':id', id ), formData, )
  }

  fetchTravelers(id:string): Observable<any> {
    return this._http.get<Destination>(this._backendURL.travelers.replace(':id', id ))
  }

  addTravelers(idDest: string, idUser: string): Observable<any> {
    return this._http.post<Destination>(this._backendURL.updateTraveler.replace(':idProperty', idUser ).replace(':id',idDest),{})

  }

  removeTravelers(idDest: string, idUser: string): Observable<any> {
    return this._http.delete(this._backendURL.updateTraveler.replace(':idProperty', idUser ).replace(':id',idDest))
  }

  addOpinion(id:string, destOp: DestinationOpinion): Observable<any> {
    return this._http.post<Destination>(this._backendURL.updateOpinions.replace(':id', id),destOp)
  }
  /**
   * Function to delete one destination for current id
   */
  delete(id: string): Observable<string> {
    return this._http.delete(this._backendURL.destination.replace(':id', id))
      .pipe(
        map(_ => id)
      );
  }

  /**
   * Function to return request options
   */
  private _options(headerList: object = {}): any {
    return { headers: new HttpHeaders(Object.assign({ 'Content-Type': 'application/json' }, headerList)) };
  }


}
