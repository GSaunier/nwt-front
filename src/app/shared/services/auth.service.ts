import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {User} from '../../models/user';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';
;



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // private property to store backendURL value
  private readonly _backendURL: any;
  // private property to store currentUserSubject value
  private _currentUserSubject: BehaviorSubject<User>;



  constructor(private readonly http: HttpClient,  public router: Router) {


    this._currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));

    this._backendURL = {};

    let baseUrl = `${environment.backend.protocol}://${environment.backend.host}`;
    if (environment.backend.port) {
      baseUrl += `:${environment.backend.port}`;
    }

    Object.keys(environment.backend.endpoints).forEach(k => this._backendURL[ k ] = `${baseUrl}${environment.backend.endpoints[ k ]}`);

  }

  // Current user
  public get currentUserValue(): User {
    return this._currentUserSubject.value;
  }


  // Sign-up
  signUp(user: User): Observable<any> {
    return this.http.post(this._backendURL.singUp, user)
      .pipe(
        catchError(this.handleError)
      );
  }

  // Sign-in
  signIn(user: User) {

    return this.http.post<any>(`${this._backendURL.singIn}`, user)
      .pipe(map( user  => {

        // set status


          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('access_token', user.token)
          localStorage.setItem('currentUser', JSON.stringify(user));
          this._currentUserSubject.next(user);

          return user;

      })).pipe(
        catchError(this.handleError)
      );
  }

  profile(): Observable<any> {
    return this.http.get<User>(`${this._backendURL.profile}`)
  }


  getToken() {
    return localStorage.getItem('access_token');
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('currentUser');
    return (authToken !== null) ? true : false;
  }

  logout() {
    localStorage.removeItem('currentUser');
    this._currentUserSubject.next(null);
  }


  // Error
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }
}







