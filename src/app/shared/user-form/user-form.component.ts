import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../models/user';
import {CustomValidators} from '../../components/registration/custom-validators';
import {Role} from '../../models/role';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit , OnChanges {

  // private property to store update mode flag
  private _isUpdateMode: boolean;
  // private property to store model value
  private _model: User;
  // private property to store cancel$ value
  private readonly _cancel$: EventEmitter<void>;
  // private property to store submit$ value
  private readonly _submit$: EventEmitter<User>;
  // private property to store user-form value
  private readonly _form: FormGroup;
  // private property to store Role values
  private _roles: any = ['admin', 'user'];
  // private property to store Selected value
  private _selected: any;

  private _profile: User;

  /**
   * Component constructor
   */
  constructor() {
    this._submit$ = new EventEmitter<User>();
    this._cancel$ = new EventEmitter<void>();
    this._form = this._buildForm();
  }

  /**
   * Sets private property _model
   */
  @Input()
  set model(model: User) {
    this._model = model;
  }


  get profile(): User {
    return this._profile;
  }

  @Input()
  set profile(value: User) {
    this._profile = value;
  }

  /**
   * Returns private property _model
   */
  get model(): User {
    return this._model;
  }

  /**
   * Returns private property _form
   */
  get form(): FormGroup {
    return this._form;
  }

  /**
   * Returns private property _isUpdateMode
   */
  get isUpdateMode(): boolean {
    return this._isUpdateMode;
  }

  /**
   * Returns private property _cancel$
   */
  @Output('cancel')
  get cancel$(): EventEmitter<void> {
    return this._cancel$;
  }

  /**
   * Returns private property _submit$
   */
  @Output('submit')
  get submit$(): EventEmitter<User> {
    return this._submit$;
  }

  /**
   * Returns private property _roles
   */
  get Roles(): any[] {
    return this._roles;
  }

  /**
   * Returns private property _selected
   */
  get selected(): Role {
    return this._selected;
  }

  /**
   * Sets private property _selected
   */
  set selected(role: Role) {
    this._selected = role;
  }

  /**
   * OnInit implementation
   */
  ngOnInit() {

  }

  /**
   * Function to handle component update
   */
  ngOnChanges(record) {

    if (record.model && record.model.currentValue) {
      this._model = record.model.currentValue;
      this._isUpdateMode = true;
      this._form.patchValue(this._model);
    } else {
      this._model = {
        pseudo: '',
        password: '',
        firstname: '',
        lastname: '',
        email: '',
        role: ''
         };
      this._isUpdateMode = false;
    }
  }

  /**
   * Function to emit event to cancel process
   */
  cancel() {
    this._cancel$.emit();
  }

  /**
   * Function to emit event to submit user-form and person
   */
  submit(user: User) {
    this._submit$.emit(user);
  }


  /**
   * Function to build our user-form
   */
  private _buildForm(): FormGroup {
    return new FormGroup({
      id: new FormControl('0'),
      pseudo: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(2)
      ])),
      password: new FormControl('', Validators.compose([
         Validators.minLength(4)
      ])),
      firstname: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(2)
      ])),
      lastname: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(2)
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required, Validators.pattern(/[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,4}/)
      ])),
      role: new FormControl('', Validators.compose([
        Validators.required
      ]))
    });
  }

}
