import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../models/user';
import {DestinationOpinion} from '../../models/destinationopinion';

@Component({
  selector: 'app-user-opinion-form',
  templateUrl: './user-opinion-form.component.html',
  styleUrls: ['./user-opinion-form.component.scss']
})
export class UserOpinionFormComponent implements OnInit {


  // private property to store model value
  private _model: string;
  // private property to store cancel$ value
  private readonly _cancel$: EventEmitter<void>;
  // private property to store submit$ value
  private readonly _submit$: EventEmitter<string>;
  // private property to store user-form value
  private readonly _form: FormGroup;


  /**
   * Component constructor
   */
  constructor() {
    this._submit$ = new EventEmitter<string>();
    this._cancel$ = new EventEmitter<void>();
    this._form = this._buildForm();
  }

  /**
   * Sets private property _model
   */
  @Input()
  set model(model: string) {
    this._model = model;
  }

  /**
   * Returns private property _model
   */
  get model(): string {
    return this._model;
  }

  /**
   * Returns private property _form
   */
  get form(): FormGroup {
    return this._form;
  }


  /**
   * Returns private property _cancel$
   */
  @Output('cancel')
  get cancel$(): EventEmitter<void> {
    return this._cancel$;
  }

  /**
   * Returns private property _submit$
   */
  @Output('submit')
  get submit$(): EventEmitter<string> {
    return this._submit$;
  }



  /**
   * OnInit implementation
   */
  ngOnInit() {

  }


  /**
   * Function to emit event to cancel process
   */
  cancel() {
    this._cancel$.emit();
  }

  /**
   * Function to emit event to submit user-form and person
   */
  submit(comment: string) {
    this._submit$.emit(comment);
  }


  /**
   * Function to build our user-form
   */
  private _buildForm(): FormGroup {
    return new FormGroup({
      comment: new FormControl('', Validators.compose([
        Validators.minLength(6)
      ]))
    });
  }

}
