import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../shared/services/auth.service';
import {CustomValidators} from './custom-validators';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  private _registrationForm: FormGroup;

  constructor(
    public fb: FormBuilder,
    public auth: AuthService,
    public router: Router
  ) {
    this._registrationForm = this._buildForm();
  }

  ngOnInit() { }


  get registerForm() {
    return this._registrationForm;
  }


  registerUser() {
    this.auth.signUp(this._registrationForm.value).subscribe((res) => {
      if (res) {
        this._registrationForm.reset()
        this.router.navigate(['/login']);
      }
    })
  }

  /**
   * Function to build our user-form
   */
  private _buildForm(): FormGroup {
    return new FormGroup({
      pseudo: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(2)
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(4)
      ])),
      firstname: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(2)
      ])),
      lastname: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(2)
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required, Validators.pattern(/[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,4}/)
      ]))
    });
  }

}



