import { FormControl } from '@angular/forms';

export class CustomValidators {

  /**
   * Function to control email with custom validator
   */
  static googleEmail(control: FormControl) {
    // returns control
    return /[A-z0-9._%+-]+@[A-z0-9.-]+.[A-z]{2,4}/.test(control.value) ? null : {
      googleEmail: true
    };
  }
}
