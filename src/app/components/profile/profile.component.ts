import { Component, OnInit } from '@angular/core';
import {User} from '../../models/user';
import {AuthService} from '../../shared/services/auth.service';
import {Observable, of} from 'rxjs';
import {flatMap, tap} from 'rxjs/operators';
import {UserService} from '../../shared/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  private _user:User;

  constructor(private _autService: AuthService, private _usersService: UserService) {
    this._user = {} as User;
  }


  get user(): User {
    return this._user;
  }

  set user(value: User) {
    this._user = value;
  }

  ngOnInit() {
    this._autService.profile().subscribe((user: User) => this._user = user, error => console.log(error));
  }

  getUpdateUser(user$: User): void {
    of(user$).pipe(
      flatMap(_ => _.password === '' ? this._deletePassword(_) : of(_)),
        flatMap((_: User) => this._usersService.update(_))
      )
        .subscribe((user: User) => this._user = user, error => console.log(error.message ))
  }

private _deletePassword(user: User): Observable<User> {

  delete user["password"];
return of(user);
}
}
