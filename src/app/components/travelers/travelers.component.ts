import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {User} from '../../models/user';
import {DestinationService} from '../../shared/services/destination.service';
import {Destination} from '../../models/destination';

@Component({
  selector: 'app-travelers',
  templateUrl: './travelers.component.html',
  styleUrls: ['./travelers.component.scss']
})
export class TravelersComponent implements OnInit{


  private _travelers: User[];
  private _destination: Destination;
  constructor(private _Dservice: DestinationService) {
      this._travelers = [];
      this._destination = {} as Destination;
  }

  /**
   * Returns private property _person
   */
  get destination(): Destination {

    return this._destination;
  }

  get travelers(): User[] {
    return this._travelers;
  }

  /**
   * Sets private property _person
   */
  @Input()
  set travelers(users: User[]) {
    console.log(users);
    this._travelers = users;
  }

  ngOnInit() {
    //this._Dservice.fetchTravelers(this.destination.id).subscribe((user: User[]) => this._travelers = user, error => console.log(error))
  }

}
