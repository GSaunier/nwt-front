import { Component, OnInit } from '@angular/core';
import {Destination} from '../../models/destination';
import {ActivatedRoute, Router} from '@angular/router';
import {DestinationService} from '../../shared/services/destination.service';
import {merge, Observable, of} from 'rxjs';
import {catchError, filter, flatMap, map, tap} from 'rxjs/operators';
import {DestinationUser} from '../../models/destinationUser';
import {DestinationImage} from '../../models/destinationImage';
import {MatDialog, MatDialogRef} from '@angular/material';
import {UserDialogComponent} from '../../shared/user-dialog/user-dialog.component';
import {User} from '../../models/user';
import {UserOpinionDialogComponent} from '../../shared/user-comment-dialog/user-opinion-dialog.component';
import {OpinionService} from '../../shared/services/opinion.service';
import {DestinationOpinion} from '../../models/destinationopinion';
import {AuthService} from '../../shared/services/auth.service';


@Component({
  selector: 'app-destination',
  templateUrl: './destination.component.html',
  styleUrls: ['./destination.component.scss']
})
export class DestinationComponent implements OnInit {

  // private property to store destination value
  private _destination: Destination;
  // private property to store travelers value
  private _travelers: DestinationUser[];
  // private property to store travelers value
  private _image: ArrayBuffer | string;
  // private property to store users value
  private _opinions: DestinationOpinion[];
  // private property to store dialogStatus value
  private _dialogStatus: string;
  // private property to store user-dialog reference
  private _commentDialog: MatDialogRef<UserOpinionDialogComponent>;

  private _user:User ;


  /**
   * Component constructor
   */
  constructor(private _opinionService: OpinionService, private  _authService: AuthService, private _DService: DestinationService, private _route: ActivatedRoute, private _router: Router, private _dialog: MatDialog) {
    this._destination = {} as Destination;
    this._user = {} as User;

  }

  /**
   * Returns private property _person
   */
  get destination(): Destination {
    return this._destination;
  }


  get user(): User {
    console.log(this._user);
    return this._user;
  }

  addTraveler(): void {
    this._DService.addTravelers(this._destination.id, this._user.id).subscribe((dest: Destination) => this._destination = dest, error =>
       console.log(error) )
  }

  removeTraveler(): void {
    this._DService.removeTravelers(this._destination.id, this._user.id).subscribe(_ => _, error =>
      console.log(error), () => this._DService.fetchOne(this._destination.id).subscribe((dest:Destination) => this._destination = dest) )
  }

  isTraveler(): boolean {
    return !!this._destination.travelers.filter(_ => _.id === this._user.id).length;
  }
  
  

  set user(value: User) {

    this._user = value;
  }

  /**
   * Returns private property _person
   */
  get opinions(): DestinationOpinion[] {
    return this._opinions;
  }


  get image() {
    return this._image;
  }

  private  _createImageFromBlob(image: Blob): void {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this._image = reader.result
    }, false);
    if(image) {
      reader.readAsDataURL(image);
    }
  }

  /**
   * OnInit implementation
   */
  ngOnInit() {
    this._route.params.pipe(
        filter(params => !!params.id),
        flatMap(params => this._DService.fetchOne(params.id))
      )
      .subscribe((destination: any) => {this._destination = destination ; this._travelers = this._destination.travelers }, error => this._goTo('/error') );
    this._route.params.pipe(
      filter(params => !!params.id),
      flatMap(params => this._DService.fetchImage(params.id))
    )
      .subscribe((image: any) => this._createImageFromBlob(image));
    this._authService.profile().subscribe((user:User) => this._user = user, error => console.log(error));

  }

  private _goTo(path: string): void {
    this._router.navigate([path]);
  }


  /**
   * Resturns private property _travelers
   */
  Travelers(): DestinationUser[] {
    return this._travelers = this.destination.travelers;
  }







  /**
   * Function to navigate to Destinations Interface
   */
  navigate() {
    this._router.navigate([ '/home/destinations']);
  }


  /**
   * Returns private property _dialogStatus
   */
  get dialogStatus(): string {
    return this._dialogStatus;
  }

  /**
   * Function to display modal
   */
  showDialog() {
    // set user-dialog status
    this._dialogStatus = 'active';

    // open modal
    this._commentDialog = this._dialog.open(UserOpinionDialogComponent, { panelClass: 'custom-dialog-container' , disableClose: true });

    // subscribe to afterClosed observable to set user-dialog status and do process
    this._commentDialog.afterClosed()
      .pipe(
        filter(_ => !!_),
        flatMap(_ => of(this._addOpinion(_.comment))),
        flatMap(_ => this._DService.addOpinion(this._destination.id,_))
      ).subscribe((dest: Destination) => this._destination = dest, error => console.log(error))

  }

  /**
   * Add new user and fetch all users to refresh the list
   */
  private _addOpinion( comment: string): DestinationOpinion {
    let user = {idUser: this._user.id,
      firstname: this._user.firstname,
      lastname: this._user.lastname,
      comment: comment,
    } as DestinationOpinion;
    console.log(user);
    return user
  }































}
