import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../models/user';
import {DestinationOpinion} from '../../models/destinationopinion';

@Component({
  selector: 'app-opinions',
  templateUrl: './opinions.component.html',
  styleUrls: ['./opinions.component.scss']
})
export class OpinionsComponent implements OnInit {

  private _opinion: DestinationOpinion;


  constructor() {
    this._opinion = {} as DestinationOpinion;
  }


  get opinion(): DestinationOpinion {
    return this._opinion;
  }

  @Input()
  set opinion(value: DestinationOpinion) {
    this._opinion = value;
  }

  ngOnInit() {
  }

}
