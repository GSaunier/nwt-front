import {Component, OnChanges, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../shared/services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnChanges {

  // private property to store user-form value
  private readonly _loginForm: FormGroup;
  // private property to store user-form value
  returnUrl: string;
  // private property to store notAllowed value
  private _notAllowed: boolean;



  constructor(private route: ActivatedRoute, private router: Router, private auth: AuthService) {

    this._loginForm = this._buildForm();


  }

  ngOnInit() {

    if(this.auth.isLoggedIn){
      if(this.auth.currentUserValue.role === 'admin'){
        this.router.navigate(['/admin/destinations']);
      }else {
        this.router.navigate(['/home/destinations']);
      }
    }else {
      this.router.navigate(['/login'])
    }

  }

  ngOnChanges(){

  }

  /**
   * convenience getter for easy access to user-form fields
   */
  get loginForm() {
    return this._loginForm;
  }

  get notAllowed() : boolean{
   return this._notAllowed;
  }


  Submit() {
    this._notAllowed = false;
    this.auth.signIn(this.loginForm.value)
            .pipe(first())
     .subscribe(
       data => {
         console.log(data);
         if(data.role === 'user' || data.role === 'admin'){
           this._notAllowed = true;
         }

         if(data.role === 'user'){
           this.router.navigate(['/home/destinations']);
         } else {
           this.router.navigate(['/admin/destinations']);
         }

       },

       (_) => { this._notAllowed = !this.auth.isLoggedIn;},
       () => { this._notAllowed = false;  }
       );
  }


  /**
   * Function to build our user-form
   */
  private _buildForm(): FormGroup {
    return new FormGroup({
      pseudo: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(2)
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(2)
      ]))
    });
  }
}
