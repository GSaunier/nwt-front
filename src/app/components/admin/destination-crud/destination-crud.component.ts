import {Component, OnChanges, OnInit, ViewChild} from '@angular/core';
import {Destination} from '../../../models/destination';
import {MatDialog, MatDialogRef, MatPaginator, MatTableDataSource} from '@angular/material';
import {DestinationService} from '../../../shared/services/destination.service';
import {Router} from '@angular/router';
import {DestinationDialogComponent} from '../../../shared/destination-dialog/destination-dialog.component';
import {filter, flatMap} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-destination-crud',
  templateUrl: './destination-crud.component.html',
  styleUrls: ['./destination-crud.component.scss']
})
export class DestinationCRUDComponent implements OnInit, OnChanges {



  /**
   * Component constructor
   */
  constructor(private _router: Router, private _DService: DestinationService, private _dialog: MatDialog) {
    this._destinations = [];
    this._dialogStatus = 'inactive';

    this._DService.fetch().subscribe(data => {
      this._destinations = data;
      this.dataSource = new MatTableDataSource<Destination>(this._destinations);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
      }, 0);
    });
  }



  /**
   * Returns private property _destinations
   */
  get destinations(): Destination[] {
    return this._destinations;
  }

  /**
   * Returns private property _dialogStatus
   */
  get dialogStatus(): string {
    return this._dialogStatus;
  }

  // private property to store destinations value
  private _destinations: Destination[];
  // private property to store dialogStatus value
  private _dialogStatus: string;
  // private property to store destination-dialog reference
  private _destinationDialog: MatDialogRef<DestinationDialogComponent>;

  dataSource: MatTableDataSource<Destination>;



  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['city', 'region', 'country', 'image',  'description', 'action'];
    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        throw new Error('Method not implemented.');

    }

  /**
   * OnInit implementation
   */
  ngOnInit() {
    this._DService
      .fetch().subscribe((destinations: Destination[]) => this._destinations = destinations);
  }


  /**
   * Function to delete one destination
   */
  deleteDestination(index: number, destination: Destination) {
    if (window.confirm('Are you sure')) {
      const data = this.dataSource.data;
      data.splice((this.paginator.pageIndex * this.paginator.pageSize) + index, 1);
      this.dataSource.data = data;
      this._DService.delete(destination.id).subscribe(_ => this._destinations = this._destinations.filter(__ => __.id !== _));
    }
  }

  /**
   * Function to display modal
   */
  showDialog(create: boolean) {
    // set destination-dialog status
    this._dialogStatus = 'active';

    // open modal
    this._destinationDialog = this._dialog.open(DestinationDialogComponent, {  panelClass: 'custom-dialog-container' , disableClose: true });

    // subscribe to afterClosed observable to set destination-dialog status and do process
    this._destinationDialog.afterClosed()
      .pipe(
        filter(_ => !!_),
        flatMap(_ => this._add(_))
      )
      .subscribe(
        (destinations: Destination[]) => {this._destinations = destinations; this.dataSource = new MatTableDataSource<Destination>(this._destinations);
          setTimeout(() => {
            this.dataSource.paginator = this.paginator;
          }, 0);},
        _ => {this._dialogStatus = 'inactive'; this._router.navigate([ '/admin/destination' ])},
        () => { this._dialogStatus = 'inactive'; this._router.navigate(['/admin/destinations'])}
      );
  }



  /**
   * Add new destination and fetch all destinations to refresh the list
   */
  private _add(destination: Destination): Observable<Destination[]> {
    return this._DService
      .create(destination)
      .pipe(
        flatMap(_ => this._DService.fetch())
      );
  }

}
