import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {DestinationDialogComponent} from '../../../shared/destination-dialog/destination-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import {DestinationService} from '../../../shared/services/destination.service';
import {filter, flatMap, map, tap} from 'rxjs/operators';
import {Destination} from '../../../models/destination';

@Component({
  selector: 'app-update-destination',
  templateUrl: './update-destination.component.html',
  styleUrls: ['./update-destination.component.scss']
})
export class UpdateDestinationComponent implements OnInit {

  // private property to store destination-dialog reference
  private _destinationsDialog: MatDialogRef<DestinationDialogComponent>;

  /**
   * Component constructor
   */
  constructor(private _route: ActivatedRoute, private _router: Router, private _DService: DestinationService, private _dialog: MatDialog) {
  }

  /**
   * OnInit implementation
   */
  ngOnInit() {

    this._route.params
      .pipe(
        map((params: any) => params.id),
        flatMap((id: string) => this._DService.fetchOne(id))
      )
      .subscribe((destination: Destination) => {
        this._destinationsDialog = this._dialog.open(DestinationDialogComponent, {
          panelClass: 'custom-dialog-container',
          disableClose: true,
          data: destination
        });

        // subscribe to afterClosed observable to set destination-dialog status and do process
        this._destinationsDialog.afterClosed()
          .pipe(
            tap(_ => console.log(_, "dsdqlmfdflm")),
            flatMap(_ => this._DService.update(_))
          ).subscribe(undefined , error => { this._router.navigate((['/admin/destinations']))}, () => this._router.navigate((['/admin/destinations'])))
      });
  }
}
