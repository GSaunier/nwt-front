import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {UserDialogComponent} from '../../../shared/user-dialog/user-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../shared/services/user.service';
import {filter, flatMap, map} from 'rxjs/operators';
import {User} from '../../../models/user';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {

  // private property to store user-dialog reference
  private _usersDialog: MatDialogRef<UserDialogComponent>;

  /**
   * Component constructor
   */
  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _dialog: MatDialog) {
  }

  /**
   * OnInit implementation
   */
  ngOnInit() {

    this._route.params
      .pipe(
        map((params: any) => params.id),
        flatMap((id: string) => this._userService.fetchOne(id))
      )
      .subscribe((user: User) => {
        this._usersDialog = this._dialog.open(UserDialogComponent, {
          panelClass: 'custom-dialog-container',
          disableClose: true,
          data: user
        });

        // subscribe to afterClosed observable to set user-dialog status and do process
        this._usersDialog.afterClosed()
          .pipe(
            filter(_ => !!_),
            flatMap(_ => _.password === '' ? this._deletePassword(_) : of(_)),
            flatMap((_: User) => this._userService.update(_))
          )
          .subscribe(undefined, error => console.log(error.message  ), () => this._router.navigate([ '/admin/users' ]));
      });
  }

  private _deletePassword(user: User): Observable<User> {

    delete user["password"];
    return of(user);
  }


}
