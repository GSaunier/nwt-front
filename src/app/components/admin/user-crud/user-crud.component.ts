import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../../../models/user';
import {MatDialog, MatDialogRef, MatPaginator, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';
import {UserService} from '../../../shared/services/user.service';
import {filter, flatMap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {UserDialogComponent} from '../../../shared/user-dialog/user-dialog.component';

@Component({
  selector: 'app-user-crud',
  templateUrl: './user-crud.component.html',
  styleUrls: ['./user-crud.component.scss']
})
export class UserCRUDComponent implements OnInit {

  // private property to store users value
  private _users: User[];
  // private property to store dialogStatus value
  private _dialogStatus: string;
  // private property to store user-dialog reference
  private _usersDialog: MatDialogRef<UserDialogComponent>;

  dataSource: MatTableDataSource<User>;



  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['pseudo', 'firstname', 'lastname', 'email',  'role', 'action'];



  /**
   * Component constructor
   */
  constructor(private _router: Router, private _userService: UserService, private _dialog: MatDialog) {
    this._users = [];
    this._dialogStatus = 'inactive';

    this._userService.fetch().subscribe(data => {
      console.log(data);
      this._users = data;
      this.dataSource = new MatTableDataSource<User>(this._users);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
      }, 0);
    })
  }



  /**
   * Returns private property _people
   */
  get users(): User[] {
    return this._users;
  }

  /**
   * Returns private property _dialogStatus
   */
  get dialogStatus(): string {
    return this._dialogStatus;
  }

  /**
   * OnInit implementation
   */
  ngOnInit() {
    this._userService
      .fetch().subscribe((users: User[]) => this._users = users);
  }

  /**
   * Function to delete one user
   */
  deleteUser(index: number, user: User) {
    if(window.confirm('Are you sure')) {
      const data = this.dataSource.data;
      data.splice((this.paginator.pageIndex * this.paginator.pageSize) + index, 1);
      this.dataSource.data = data;
      this._userService.delete(user.id).subscribe(_ => this._users = this._users.filter(__ => __.id !== _));
    }
  }

  /**
   * Function to display modal
   */
  showDialog() {
    // set user-dialog status
    this._dialogStatus = 'active';

    // open modal
    this._usersDialog = this._dialog.open(UserDialogComponent, { panelClass: 'custom-dialog-container' , disableClose: true });

    // subscribe to afterClosed observable to set user-dialog status and do process
    this._usersDialog.afterClosed()
      .pipe(
        filter(_ => !!_),
        flatMap(_ => this._add(_))
      )
      .subscribe(
        (users: User[]) => {this._users = users; this.dataSource = new MatTableDataSource<User>(this._users);
          setTimeout(() => {
            this.dataSource.paginator = this.paginator;
          }, 0);},
        _ => {this._dialogStatus = 'inactive'; },
        () => {this._dialogStatus = 'inactive';
                        this._router.navigate([ '/admin/users' ])}
      );
  }

  /**
   * Add new user and fetch all users to refresh the list
   */
  private _add(user: User): Observable<User[]> {
    return this._userService
      .create(user)
      .pipe(
        flatMap(_ => this._userService.fetch())
      );
  }

}
