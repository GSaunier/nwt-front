import { Component, OnInit } from '@angular/core';
import {Destination} from '../../models/destination';
import {Router} from '@angular/router';
import {DestinationService} from '../../shared/services/destination.service';
import {User} from '../../models/user';
import {filter, flatMap, map} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-destinations',
  templateUrl: './destinations.component.html',
  styleUrls: ['./destinations.component.scss']
})
export class DestinationsComponent implements OnInit {

  // private property to store people value
  private _destinations: Destination[];
  private _images: ArrayBuffer[] | string[];


  /**
   * Component constructor
   */
  constructor(private _router: Router, private _dService: DestinationService) {
    this._destinations = [];
    this._images = [];

  }

  image(id: string) {
    return this._images[id];
  }

  private  _createImageFromBlob(image: Blob, id: string) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this._images[id] = reader.result
    }, false);
    if(image) {
      reader.readAsDataURL(image);
    }
  }


  /**
   * Returns private property _people
   */
  get destinations(): Destination[] {
    return this._destinations;
  }

  /**
   * OnInit implementation
   */
  ngOnInit() {
    this._dService
      .fetch().subscribe((destinations: Destination[]) => this.fetchImages(destinations));
  }

  fetchImages(destinations: Destination[]) {
    this._destinations = destinations;
    this._destinations.map(_ => this._dService.fetchImage(_.id).pipe(
    ).subscribe((image: any) => this._createImageFromBlob(image,_.id), error => console.log(error)))
  }


  /**
   * Function to navigate to current person
   */
  navigate(destination: Destination) {
    this._router.navigate([ '/home/destination', destination.id ]);
  }

 // get numberOfTravlers() {

  //}

}
