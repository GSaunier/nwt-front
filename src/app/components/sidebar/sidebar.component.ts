import { Component, OnInit } from '@angular/core';
import {User} from '../../models/user';
import {AuthService} from '../../shared/services/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {



  constructor(private _authService : AuthService) {

  }

  ngOnInit() {
  }

  get currentUser(): User{
    return this._authService.currentUserValue;
  }

}
