import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RegistrationComponent } from './components/registration/registration.component';
import { HomeComponent } from './components/home/home.component';
import { AdminComponent } from './components/admin/admin.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ErrorInterceptor} from './auth/interceptors/error.interceptor';
import {JwtInterceptor} from './auth/interceptors/jwt.interceptor';
import {DestinationComponent} from './components/destination/destination.component';
import {DestinationsComponent} from './components/destinations/destinations.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {GroupsComponent} from './components/groups/groups.component';
import {UserCRUDComponent} from './components/admin/user-crud/user-crud.component';
import {DestinationCRUDComponent} from './components/admin/destination-crud/destination-crud.component';
import {UserDialogComponent} from './shared/user-dialog/user-dialog.component';
import {UserFormComponent} from './shared/user-form/user-form.component';
import {MatDialogModule} from '@angular/material';
import {UpdateUserComponent} from './components/admin/update-user/update-user.component';
import {DestinationDialogComponent} from './shared/destination-dialog/destination-dialog.component';
import {DestinationFormComponent} from './shared/destination-form/destination-form.component';
import {UpdateDestinationComponent} from './components/admin/update-destination/update-destination.component';
import {MaterialFileInputModule} from 'ngx-material-file-input';
import {UserOpinionDialogComponent} from './shared/user-comment-dialog/user-opinion-dialog.component';
import {UserOpinionFormComponent} from './shared/user-opinion-form/user-opinion-form.component';
import {ErrorComponent} from './components/error/error.component';
import {TravelersComponent} from './components/travelers/travelers.component';
import { OpinionsComponent } from './components/opinions/opinions.component';
import { ProfileComponent } from './components/profile/profile.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    HomeComponent,
    AdminComponent,
    DestinationComponent,
    DestinationsComponent,
    SidebarComponent,
    GroupsComponent,
    UserCRUDComponent,
    DestinationCRUDComponent,
    UserDialogComponent,
    UserFormComponent,
    UpdateUserComponent,
    DestinationDialogComponent,
    DestinationFormComponent,
    UpdateDestinationComponent,
    ErrorComponent,
    UserOpinionDialogComponent,
    UserOpinionFormComponent,
    TravelersComponent,
    OpinionsComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDialogModule,
    MaterialFileInputModule
  ],
  entryComponents: [ UserDialogComponent , DestinationDialogComponent, UserOpinionDialogComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
