import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {HomeComponent} from './components/home/home.component';
import {AdminComponent} from './components/admin/admin.component';
import {AuthGuard} from './auth/guards/auth.guard';
import {Role} from './models/role';
import {DestinationsComponent} from './components/destinations/destinations.component';
import {GroupsComponent} from './components/groups/groups.component';
import {DestinationComponent} from './components/destination/destination.component';
import {DestinationCRUDComponent} from './components/admin/destination-crud/destination-crud.component';
import {UserCRUDComponent} from './components/admin/user-crud/user-crud.component';
import {UserFormComponent} from './shared/user-form/user-form.component';
import {UpdateUserComponent} from './components/admin/update-user/update-user.component';
import {UpdateDestinationComponent} from './components/admin/update-destination/update-destination.component';
import {ErrorComponent} from './components/error/error.component';
import {ProfileComponent} from './components/profile/profile.component';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', redirectTo: 'home/destinations', pathMatch: 'full' },
  { path: 'admin', redirectTo: 'admin/destinations', pathMatch: 'full' },
  { path: 'error', component: ErrorComponent },

  { path: 'home',  component: HomeComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'destinations',
        component: DestinationsComponent
      },
      {
        path: 'destination/:id',
        component: DestinationComponent
      },
      {
        path: 'groups',
        component: GroupsComponent
      },
      {
        path: 'profile',
        component: ProfileComponent
      }
    ]
  },
  { path: 'admin', component: AdminComponent ,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin] },
    children: [
      {
        path: 'destinations',
        component: DestinationCRUDComponent
      },
      {
        path: 'users',
        component: UserCRUDComponent
      },
      { path: 'editUser/:id', component: UpdateUserComponent },
      { path: 'editDestination/:id', component: UpdateDestinationComponent },
    ]
    },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegistrationComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: 'error', pathMatch: "full" }

];

@NgModule({
  imports: [RouterModule.forRoot(routes ,{ useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
