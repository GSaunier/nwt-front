export class DestinationUser {
  id: string;
  firstname: string;
  lastname: string;
  email: string;
}
