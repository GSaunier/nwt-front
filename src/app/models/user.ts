export class User {
  id?: string;
  pseudo: string;
  password: string;
  firstname: string;
  lastname: string;
  email: string;
  role: string;
  token?: string;
}

