import {DestinationUser} from './destinationUser';
import {DestinationImage} from './destinationImage';
import {DestinationOpinion} from './destinationopinion';

export class Destination {
  id?: string;
  country: string;
  region: string;
  description?: string;
  city: string;
  image?: DestinationImage;
  opinions?: DestinationOpinion[];
  contacts?: DestinationUser[];
  travelers?: DestinationUser[];
}
